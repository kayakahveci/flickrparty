//
//  PhotoDetailView.m
//  FlickrParty
//
//  Created by Kaya Kahveci on 28/03/16.
//  Copyright © 2016 Kaya Kahveci. All rights reserved.
//

#import "PhotoDetailView.h"
#import "DataModels.h"
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"

@implementation PhotoDetailView{
    UIView *photoView; //main view
    UIImageView *imgPhoto; //photo to be zoomed
    UIScrollView *svPhoto; //for zooming
}
-(void)showPhotoDetailView:(id)photoObj{
    //our photo object
    FlickrPhoto *photo = (FlickrPhoto *)photoObj;
    
    CGRect viewFrame = [UIApplication sharedApplication].keyWindow.bounds;
    photoView = [[UIView alloc] initWithFrame:viewFrame];
    
    //transparent background
    UIImageView *imgBack = [[UIImageView alloc] initWithFrame:photoView.frame];
    imgBack.backgroundColor = [UIColor blackColor];
    imgBack.alpha = 0.9;
    [photoView addSubview:imgBack];
    
    //x button to close view
    UIButton *btnClose = [[UIButton alloc] initWithFrame:CGRectMake(0, 20, 40, 40)];
    [btnClose.titleLabel setTextColor:[UIColor whiteColor]];
    [btnClose.titleLabel setFont:[UIFont boldSystemFontOfSize:32]];
    [btnClose setTitle:@"X" forState:UIControlStateNormal];
    [btnClose addTarget:self action:@selector(closeView) forControlEvents:UIControlEventTouchUpInside];
    [photoView addSubview:btnClose];
    
    //'b' in the url is for large photo. for details: https://www.flickr.com/services/api/misc.urls.html
    imgPhoto = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, viewFrame.size.width, viewFrame.size.width)];
    imgPhoto.contentMode = UIViewContentModeScaleAspectFit;
    NSURL *photoURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://farm%@.static.flickr.com/%@/%@_%@_b.jpg",
     photo.farm, photo.server,
     photo.photoIdentifier, photo.secret]];
    [imgPhoto setImageWithURL:photoURL
             placeholderImage:nil usingActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    
    //label height that should be
    float labelHeight = [self getTextSize:photo.title width:photoView.frame.size.width-16];
    //label of the photo title
    UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(8, photoView.frame.size.height-labelHeight-8, photoView.frame.size.width-16, labelHeight)];
    lblTitle.text = photo.title;
    lblTitle.numberOfLines = 3;
    lblTitle.textColor = [UIColor whiteColor];
    [photoView addSubview:lblTitle];
    
    //scrollview for zooming
    svPhoto = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 60, viewFrame.size.width, viewFrame.size.height-lblTitle.frame.size.height-60-8)];
    imgPhoto.layer.position = CGPointMake(svPhoto.frame.size.width/2, svPhoto.frame.size.height/2);
    svPhoto.contentSize = CGSizeMake(imgPhoto.image.size.width, imgPhoto.image.size.height);
    svPhoto.maximumZoomScale = 5;
    svPhoto.minimumZoomScale = 1;
    svPhoto.clipsToBounds = YES;
    svPhoto.delegate = self;
    [svPhoto addSubview:imgPhoto];
    [photoView addSubview:svPhoto];
    
    UIWindow* currentWindow = [UIApplication sharedApplication].keyWindow;
    [currentWindow addSubview:photoView];
    [currentWindow bringSubviewToFront:photoView];
}
//view to be zoomed
-(UIView *) viewForZoomingInScrollView:(UIScrollView *)inScroll {
    return imgPhoto;
}
//re-center the contents
- (void)centerScrollViewContents {
    CGSize boundsSize = svPhoto.bounds.size;
    CGRect contentsFrame = imgPhoto.frame;
    
    if (contentsFrame.size.width < boundsSize.width) {
        contentsFrame.origin.x = (boundsSize.width - contentsFrame.size.width) / 2.0f;
    } else {
        contentsFrame.origin.x = 0.0f;
    }
    
    if (contentsFrame.size.height < boundsSize.height) {
        contentsFrame.origin.y = (boundsSize.height - contentsFrame.size.height) / 2.0f;
    } else {
        contentsFrame.origin.y = 0.0f;
    }
    
    imgPhoto.frame = contentsFrame;
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView {
    // The scroll view has zoomed, so we need to re-center the contents
    [self centerScrollViewContents];
}
//when clicked x button
-(void)closeView{
    [photoView removeFromSuperview];
}
//it gives the height of label that should be
-(float )getTextSize :(NSString *)text width:(float)textWidth
{
    NSAttributedString *attributedText =
    [[NSAttributedString alloc]
     initWithString:text
     attributes:@
     {
     NSFontAttributeName: [UIFont systemFontOfSize:17]
     }];
    
    CGRect rect = [attributedText boundingRectWithSize:(CGSize){textWidth, CGFLOAT_MAX}
                                               options:NSStringDrawingUsesLineFragmentOrigin
                                               context:nil];
    CGSize finalSize = rect.size;
    return finalSize.height;
}

@end

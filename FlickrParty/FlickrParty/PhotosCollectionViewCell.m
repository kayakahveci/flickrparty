//
//  PhotosCollectionViewCell.m
//  FlickrParty
//
//  Created by Kaya Kahveci on 28/03/16.
//  Copyright © 2016 Kaya Kahveci. All rights reserved.
//

#import "PhotosCollectionViewCell.h"

@implementation PhotosCollectionViewCell
@synthesize imgPhoto,lblTitle;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = [UIColor clearColor];
        
        // set content view
        CGRect frame  = self.bounds;
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:frame];
        imgPhoto = imageView;
        imgPhoto.contentMode = UIViewContentModeCenter;
        imgPhoto.clipsToBounds = YES;
        [self.contentView addSubview:imgPhoto];
        
        // set transparent background for title label
        UIView *labelBG = [[UIView alloc] initWithFrame:CGRectMake(0, frame.size.height-20, frame.size.width, 20)];
        labelBG.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.5];
        lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(4, 0, frame.size.width-8, 20)];
        lblTitle.textColor = [UIColor whiteColor];
        lblTitle.font = [UIFont systemFontOfSize:14.0];
        [labelBG addSubview:lblTitle];
        [self.contentView addSubview:labelBG];
        
    }
    return self;
}
@end

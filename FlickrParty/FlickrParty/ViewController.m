//
//  ViewController.m
//  FlickrParty
//
//  Created by Kaya Kahveci on 28/03/16.
//  Copyright © 2016 Kaya Kahveci. All rights reserved.
//

#import "ViewController.h"
#import "PhotosCollectionViewCell.h"
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"
#import "FlickrSearchRequest.h"
#import "DataModels.h"
#import "PhotoDetailView.h"

@interface ViewController (){
    NSMutableArray  *photoList; //to store photo array that we parsed.
    UIActivityIndicatorView *loading;
    UISearchBar *search;
    UICollectionView *clvPhotos;
    PhotoDetailView *photoDetail;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    photoList = [[NSMutableArray alloc] init];
    photoDetail = [[PhotoDetailView alloc] init];

    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
    CGRect viewFrame = self.view.frame;
    
    //for status background color
    UIView *statusBarBG = [[UIView alloc] initWithFrame:CGRectMake(0, 0, viewFrame.size.width, 20)];
    statusBarBG.backgroundColor = [UIColor colorWithRed:0/255.0 green:99/255.0 blue:220/255.0 alpha:1.0];    //status background color as flickrblue
    [self.view addSubview:statusBarBG];
    
    //searchbar for another tags
    search = [[UISearchBar alloc] init];
    [search setTintColor:[UIColor colorWithRed:233.0/255.0
                                         green:233.0/255.0
                                          blue:233.0/255.0
                                         alpha:1.0]];
    search.frame = CGRectMake(0, 20, viewFrame.size.width,50);
    search.placeholder = @"A comma-delimited list of tags";
    search.barTintColor = [UIColor colorWithRed:0/255.0 green:99/255.0 blue:220/255.0 alpha:1.0]; //search background color as flickrblue
    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setTintColor:[UIColor colorWithRed:255/255.0 green:0/255.0 blue:132/255.0 alpha:1.0]];
    search.text = @"Party";
    search.delegate = self;
    [self.view addSubview:search];
    
    //without status and search height
    viewFrame.origin.y += 70;
    viewFrame.size.height -= 70;
    
    //photo collection view 
    clvPhotos=[[UICollectionView alloc] initWithFrame:viewFrame collectionViewLayout:layout];
    [clvPhotos setDataSource:self];
    [clvPhotos setDelegate:self];
    [clvPhotos registerClass:[PhotosCollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [clvPhotos setBackgroundColor:[UIColor colorWithRed:233/255.0 green:233/255.0 blue:233/255.0 alpha:1.0]];
    [self.view addSubview:clvPhotos];
    
    //activity indicator for loading op.
    loading = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    loading.frame = CGRectMake(0, 0, 80, 80);
    loading.backgroundColor = [UIColor colorWithRed:255/255.0 green:0/255.0 blue:132/255.0 alpha:1.0]; //flickr pink
    loading.layer.cornerRadius = 15;
    loading.layer.position = self.view.layer.position;
    [self.view addSubview:loading];
    [loading setHidesWhenStopped:YES];
    [loading setHidden:YES];
    
    //search for party with max 100results
    [self search:search.text perPage:100];
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return photoList.count;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    PhotosCollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    cell.backgroundColor = [UIColor clearColor];
    //flickrphoto from the list we parsed
    FlickrPhoto *photo = (FlickrPhoto *)[photoList objectAtIndex:indexPath.row];
    
    //valid flickr photo URL
    NSURL *photoURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://farm%@.static.flickr.com/%@/%@_%@_z.jpg",
     photo.farm, photo.server,
     photo.photoIdentifier, photo.secret]];
    [cell.imgPhoto setImageWithURL:photoURL
                  placeholderImage:[UIImage imageNamed:@"FlickrSmall"] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    cell.lblTitle.text = photo.title;
    return cell;
}

#pragma mark Collection view layout

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 8.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 8.0;
}

// Layout: Set Edges
- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(8,8,8,8);  // top, left, bottom, right
}
// Layout: Set Item Size
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    float size = (self.view.frame.size.width-24)/2;
    return CGSizeMake(size, size);
}
// Show photo detail
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [search resignFirstResponder];
    [photoDetail showPhotoDetailView:[photoList objectAtIndex:indexPath.row]];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
// Get search data to an array
-(void)search:(NSString *)tag perPage:(int)number{
    [loading setHidden:NO];
    [loading startAnimating];
    FlickrSearchRequest *navReq = [[FlickrSearchRequest alloc] init];
    [navReq getPhotos:tag perPage:number block:^(NSDictionary *results){
        [loading stopAnimating];
        [photoList removeAllObjects];
        FlickrSearch *flSearch = [[FlickrSearch alloc] initWithDictionary:results];
        
        // an array from the dictionary for easy access to each post
        NSArray *photos = flSearch.photos.photo;
        [photoList addObjectsFromArray:photos];
        [clvPhotos reloadData];
    }];
}
// Search Clicked
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    //replace spaces for valid url
    [self search:[searchBar.text stringByReplacingOccurrencesOfString:@" " withString:@"+"] perPage:100];
    [searchBar resignFirstResponder];
}


@end

//
//  FlickrPhoto.m
//
//  Created by   on 27/03/16
//  Copyright (c) 2016 Kaya Kahveci. All rights reserved.
//

#import "FlickrPhoto.h"


NSString *const kFlickrPhotoSecret = @"secret";
NSString *const kFlickrPhotoOwner = @"owner";
NSString *const kFlickrPhotoFarm = @"farm";
NSString *const kFlickrPhotoId = @"id";
NSString *const kFlickrPhotoServer = @"server";
NSString *const kFlickrPhotoTitle = @"title";
NSString *const kFlickrPhotoIsfriend = @"isfriend";
NSString *const kFlickrPhotoIsfamily = @"isfamily";
NSString *const kFlickrPhotoIspublic = @"ispublic";


@interface FlickrPhoto ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation FlickrPhoto

@synthesize secret = _secret;
@synthesize owner = _owner;
@synthesize farm = _farm;
@synthesize photoIdentifier = _photoIdentifier;
@synthesize server = _server;
@synthesize title = _title;
@synthesize isfriend = _isfriend;
@synthesize isfamily = _isfamily;
@synthesize ispublic = _ispublic;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
        self.secret = [self objectOrNilForKey:kFlickrPhotoSecret fromDictionary:dict];
        self.owner = [self objectOrNilForKey:kFlickrPhotoOwner fromDictionary:dict];
        self.farm = [self objectOrNilForKey:kFlickrPhotoFarm fromDictionary:dict];
        self.photoIdentifier = [self objectOrNilForKey:kFlickrPhotoId fromDictionary:dict];
        self.server = [self objectOrNilForKey:kFlickrPhotoServer fromDictionary:dict];
        self.title = [self objectOrNilForKey:kFlickrPhotoTitle fromDictionary:dict];
        self.isfriend = [self objectOrNilForKey:kFlickrPhotoIsfriend fromDictionary:dict];
        self.isfamily = [self objectOrNilForKey:kFlickrPhotoIsfamily fromDictionary:dict];
        self.ispublic = [self objectOrNilForKey:kFlickrPhotoIspublic fromDictionary:dict];
        
    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.secret forKey:kFlickrPhotoSecret];
    [mutableDict setValue:self.owner forKey:kFlickrPhotoOwner];
    [mutableDict setValue:self.farm forKey:kFlickrPhotoFarm];
    [mutableDict setValue:self.photoIdentifier forKey:kFlickrPhotoId];
    [mutableDict setValue:self.server forKey:kFlickrPhotoServer];
    [mutableDict setValue:self.title forKey:kFlickrPhotoTitle];
    [mutableDict setValue:self.isfriend forKey:kFlickrPhotoIsfriend];
    [mutableDict setValue:self.isfamily forKey:kFlickrPhotoIsfamily];
    [mutableDict setValue:self.ispublic forKey:kFlickrPhotoIspublic];
    
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    
    self.secret = [aDecoder decodeObjectForKey:kFlickrPhotoSecret];
    self.owner = [aDecoder decodeObjectForKey:kFlickrPhotoOwner];
    self.farm = [aDecoder decodeObjectForKey:kFlickrPhotoFarm];
    self.photoIdentifier = [aDecoder decodeObjectForKey:kFlickrPhotoId];
    self.server = [aDecoder decodeObjectForKey:kFlickrPhotoServer];
    self.title = [aDecoder decodeObjectForKey:kFlickrPhotoTitle];
    self.isfriend = [aDecoder decodeObjectForKey:kFlickrPhotoIsfriend];
    self.isfamily = [aDecoder decodeObjectForKey:kFlickrPhotoIsfamily];
    self.ispublic = [aDecoder decodeObjectForKey:kFlickrPhotoIspublic];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    
    [aCoder encodeObject:_secret forKey:kFlickrPhotoSecret];
    [aCoder encodeObject:_owner forKey:kFlickrPhotoOwner];
    [aCoder encodeObject:_farm forKey:kFlickrPhotoFarm];
    [aCoder encodeObject:_photoIdentifier forKey:kFlickrPhotoId];
    [aCoder encodeObject:_server forKey:kFlickrPhotoServer];
    [aCoder encodeObject:_title forKey:kFlickrPhotoTitle];
    [aCoder encodeObject:_isfriend forKey:kFlickrPhotoIsfriend];
    [aCoder encodeObject:_isfamily forKey:kFlickrPhotoIsfamily];
    [aCoder encodeObject:_ispublic forKey:kFlickrPhotoIspublic];
}

- (id)copyWithZone:(NSZone *)zone
{
    FlickrPhoto *copy = [[FlickrPhoto alloc] init];
    
    if (copy) {
        
        copy.secret = [self.secret copyWithZone:zone];
        copy.owner = [self.owner copyWithZone:zone];
        copy.farm = self.farm;
        copy.photoIdentifier = [self.photoIdentifier copyWithZone:zone];
        copy.server = [self.server copyWithZone:zone];
        copy.title = [self.title copyWithZone:zone];
        copy.isfriend = self.isfriend;
        copy.isfamily = self.isfamily;
        copy.ispublic = self.ispublic;
    }
    
    return copy;
}


@end

//
//  FlickrSearch.m
//
//  Created by   on 28/03/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "FlickrSearch.h"
#import "FlickrPhotos.h"


NSString *const kFlickrSearchStat = @"stat";
NSString *const kFlickrSearchPhotos = @"photos";


@interface FlickrSearch ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation FlickrSearch

@synthesize stat = _stat;
@synthesize photos = _photos;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.stat = [self objectOrNilForKey:kFlickrSearchStat fromDictionary:dict];
            self.photos = [FlickrPhotos modelObjectWithDictionary:[dict objectForKey:kFlickrSearchPhotos]];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.stat forKey:kFlickrSearchStat];
    [mutableDict setValue:[self.photos dictionaryRepresentation] forKey:kFlickrSearchPhotos];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.stat = [aDecoder decodeObjectForKey:kFlickrSearchStat];
    self.photos = [aDecoder decodeObjectForKey:kFlickrSearchPhotos];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_stat forKey:kFlickrSearchStat];
    [aCoder encodeObject:_photos forKey:kFlickrSearchPhotos];
}

- (id)copyWithZone:(NSZone *)zone
{
    FlickrSearch *copy = [[FlickrSearch alloc] init];
    
    if (copy) {

        copy.stat = [self.stat copyWithZone:zone];
        copy.photos = [self.photos copyWithZone:zone];
    }
    
    return copy;
}


@end

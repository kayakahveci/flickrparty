//
//  FlickrSearchRequest.m
//  FlickrParty
//
//  Created by Kaya Kahveci on 28/03/16.
//  Copyright © 2016 Kaya Kahveci. All rights reserved.
//

#import "FlickrSearchRequest.h"
#import "AFNetworking.h"

#define FlickrApiKey @"65f248937e2547546cb3a1d2073b7cbb"

@implementation FlickrSearchRequest

-(void)getPhotos:(NSString *)tag perPage:(int)perPage block:(void (^)(NSDictionary *))handlerBlock{
    NSString *urlString = [[NSString alloc] initWithFormat:@"https:api.flickr.com/services/rest/?method=flickr.photos.search&api_key=%@&tags=%@&per_page=%d&format=json&nojsoncallback=1",FlickrApiKey,tag,perPage];
    AFHTTPSessionManager *operationManager = [AFHTTPSessionManager manager];
    operationManager.requestSerializer = [AFJSONRequestSerializer serializer];
    //[operationManager.requestSerializer setValue:@"" forHTTPHeaderField:@""];
    [operationManager GET:urlString parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject){        handlerBlock((NSDictionary *)responseObject);
        resultHandlerBlock = handlerBlock;
    }failure:^(NSURLSessionTask *operation, NSError *error){
        NSLog(@"error %@",error);
    }];
}

@end

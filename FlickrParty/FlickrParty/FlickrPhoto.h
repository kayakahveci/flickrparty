//
//  FlickrPhoto.h
//
//  Created by   on 28/03/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface FlickrPhoto : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *secret;
@property (nonatomic, strong) NSString *owner;
@property (nonatomic, assign) NSNumber *farm;
@property (nonatomic, strong) NSString *photoIdentifier;
@property (nonatomic, strong) NSString *server;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, assign) NSNumber *isfriend;
@property (nonatomic, assign) NSNumber *isfamily;
@property (nonatomic, assign) NSNumber *ispublic;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end

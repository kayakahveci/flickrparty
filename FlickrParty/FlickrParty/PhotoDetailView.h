//
//  PhotoDetailView.h
//  FlickrParty
//
//  Created by Kaya Kahveci on 28/03/16.
//  Copyright © 2016 Kaya Kahveci. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface PhotoDetailView : NSObject<UIScrollViewDelegate>
-(void)showPhotoDetailView:(id)photoObj;

@end

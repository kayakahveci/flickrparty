//
//  FlickrSearchRequest.h
//  FlickrParty
//
//  Created by Kaya Kahveci on 28/03/16.
//  Copyright © 2016 Kaya Kahveci. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FlickrSearchRequest : NSObject{
void(^resultHandlerBlock)(NSDictionary *results);
}
-(void)getPhotos:(NSString *)tag perPage:(int)perPage block:(void (^)(NSDictionary *))handlerBlock;
@end

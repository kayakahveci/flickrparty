//
//  FlickrPhotos.m
//
//  Created by   on 28/03/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "FlickrPhotos.h"
#import "FlickrPhoto.h"


NSString *const kFlickrPhotosPhoto = @"photo";
NSString *const kFlickrPhotosPages = @"pages";
NSString *const kFlickrPhotosPerpage = @"perpage";
NSString *const kFlickrPhotosTotal = @"total";
NSString *const kFlickrPhotosPage = @"page";


@interface FlickrPhotos ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation FlickrPhotos

@synthesize photo = _photo;
@synthesize pages = _pages;
@synthesize perpage = _perpage;
@synthesize total = _total;
@synthesize page = _page;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
    NSObject *receivedFlickrPhoto = [dict objectForKey:kFlickrPhotosPhoto];
    NSMutableArray *parsedFlickrPhoto = [NSMutableArray array];
    if ([receivedFlickrPhoto isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedFlickrPhoto) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedFlickrPhoto addObject:[FlickrPhoto modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedFlickrPhoto isKindOfClass:[NSDictionary class]]) {
       [parsedFlickrPhoto addObject:[FlickrPhoto modelObjectWithDictionary:(NSDictionary *)receivedFlickrPhoto]];
    }

    self.photo = [NSArray arrayWithArray:parsedFlickrPhoto];
            self.pages = [[self objectOrNilForKey:kFlickrPhotosPages fromDictionary:dict] doubleValue];
            self.perpage = [[self objectOrNilForKey:kFlickrPhotosPerpage fromDictionary:dict] doubleValue];
            self.total = [self objectOrNilForKey:kFlickrPhotosTotal fromDictionary:dict];
            self.page = [[self objectOrNilForKey:kFlickrPhotosPage fromDictionary:dict] doubleValue];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    NSMutableArray *tempArrayForPhoto = [NSMutableArray array];
    for (NSObject *subArrayObject in self.photo) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForPhoto addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForPhoto addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForPhoto] forKey:kFlickrPhotosPhoto];
    [mutableDict setValue:[NSNumber numberWithDouble:self.pages] forKey:kFlickrPhotosPages];
    [mutableDict setValue:[NSNumber numberWithDouble:self.perpage] forKey:kFlickrPhotosPerpage];
    [mutableDict setValue:self.total forKey:kFlickrPhotosTotal];
    [mutableDict setValue:[NSNumber numberWithDouble:self.page] forKey:kFlickrPhotosPage];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.photo = [aDecoder decodeObjectForKey:kFlickrPhotosPhoto];
    self.pages = [aDecoder decodeDoubleForKey:kFlickrPhotosPages];
    self.perpage = [aDecoder decodeDoubleForKey:kFlickrPhotosPerpage];
    self.total = [aDecoder decodeObjectForKey:kFlickrPhotosTotal];
    self.page = [aDecoder decodeDoubleForKey:kFlickrPhotosPage];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_photo forKey:kFlickrPhotosPhoto];
    [aCoder encodeDouble:_pages forKey:kFlickrPhotosPages];
    [aCoder encodeDouble:_perpage forKey:kFlickrPhotosPerpage];
    [aCoder encodeObject:_total forKey:kFlickrPhotosTotal];
    [aCoder encodeDouble:_page forKey:kFlickrPhotosPage];
}

- (id)copyWithZone:(NSZone *)zone
{
    FlickrPhotos *copy = [[FlickrPhotos alloc] init];
    
    if (copy) {

        copy.photo = [self.photo copyWithZone:zone];
        copy.pages = self.pages;
        copy.perpage = self.perpage;
        copy.total = [self.total copyWithZone:zone];
        copy.page = self.page;
    }
    
    return copy;
}


@end
